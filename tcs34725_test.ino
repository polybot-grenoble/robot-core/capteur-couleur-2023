#include <Wire.h>
#include "Adafruit_TCS34725.h"
#include "color.cpp"
#define NCAPTEURS 4

#define SENSORS_PER_MULTIPLEXOR 2


byte multiAddress = 0x70;

//déclar objets couleurs
RGB colorRGB = RGB(0, 0, 0);
HSL colorHSL = HSL(0, 0, 0);

Adafruit_TCS34725 tcs[] = { 
    Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_300MS, TCS34725_GAIN_1X), 
    Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_300MS, TCS34725_GAIN_1X),
    Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_300MS, TCS34725_GAIN_1X),
    Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_300MS, TCS34725_GAIN_1X),
    Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_300MS, TCS34725_GAIN_1X),
    Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_300MS, TCS34725_GAIN_1X),
    Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_300MS, TCS34725_GAIN_1X),
    Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_300MS, TCS34725_GAIN_1X),
    Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_300MS, TCS34725_GAIN_1X),
    Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_300MS, TCS34725_GAIN_1X),
    Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_300MS, TCS34725_GAIN_1X),
    Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_300MS, TCS34725_GAIN_1X),
};


void setup(void) {
    Serial.begin(9600);
    Wire.begin();
  
    char i;

    for(i = 0; i<NCAPTEURS; i++){
        choose_sensor(i);
        if (tcs[i].begin()) {
            Serial.println("Found sensor");
        } else {
            Serial.println("No TCS34725 found ... check your connections");
            while (1);
        }
    }
}

void loop(void) {
    float r, g, b;
    char i;

    String result;

    for(i = 0; i<NCAPTEURS; i++){
        choose_sensor(i);

        tcs[i].getRGB(&r, &g, &b);

        colorRGB.R = (int)r;
        colorRGB.G = (int)g;
        colorRGB.B = (int)b;

        colorHSL = RGBToHSL(colorRGB);

        int colorID = identifyColor(colorHSL);

        switch(colorID){
            case 1:
                result = "Marron";
                break;
            case 2:
                result = "Jaune";
                break;
            case 3:
                result = "Rose";
                break;
            case 0:
            default:
                result = "Rien";
                break;
        }




        Serial.print(" H: "); Serial.print(colorHSL.H); Serial.print(" ");
        Serial.print("S: "); Serial.print(colorHSL.S); Serial.print(" ");
        Serial.print("L: "); Serial.print(colorHSL.L); Serial.print(" ");

        Serial.print(i);

        Serial.print(" Couleur : "); Serial.print(result);

    }
    
        Serial.println();
}



void chooseBus(uint8_t multiplexor, uint8_t bus){
    Wire.beginTransmission(0x70+(multiplexor&1));
    Wire.write(1 << (bus+2));
    Wire.endTransmission();
}

void disableMultiplexor(uint8_t multiplexor){
    Wire.beginTransmission(0x70+(multiplexor&1));
    Wire.write(0);
    Wire.endTransmission();
}


void choose_sensor(uint8_t id_sensor){
    uint8_t multiplexor = id_sensor / SENSORS_PER_MULTIPLEXOR;
    uint8_t subaddress = id_sensor % SENSORS_PER_MULTIPLEXOR;
    chooseBus(multiplexor, subaddress);
    disableMultiplexor(1-multiplexor);
}